<!-- Section  -->
<section class="background-space__creation">
    <section class="bloc-page flex-grid space__creation-content">
        <section class="width50 space__creation-content--text">
            <h4>Création d'espace remarquable</h4>
            <p>Vous désirez faire appel à l'un de nos service? N'hésitez pas à nous contacter!</p>
        </section>
        <a href="http://vertlette.dev.local/contact/" class="space__creation-content--style-bottom">Contactez-nous</a>
    </section>
</section>
</main><!-- #primary-->