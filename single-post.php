<?php get_header(); ?>

<main id="primary" class="site-main">
    <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
            <section class="flex-grid prestige-content padding__post">
                <section class="bloc-page">
                    <h4 class="color__title"><?php the_title() ?></h4>
                    <article class="prestige-content--post__type__description">
                        <?php the_content() ?>
                    </article>
                    <a href="<?= home_url('/'); ?>" class="back-button">Retour</a>
                </section>
            </section>
        <?php endwhile ?>
    <?php endif; ?>
</main><!-- #primary-->

<?php get_footer(); ?>