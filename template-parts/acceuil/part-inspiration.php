<!-- Section  -->
<?php if (have_posts($post = 108)) : ?>
    <section class="flex-grid inspiration-content">
        <section class="inspiration-content--width50">
            <?php the_post_thumbnail() ?>
        </section>
        <section class="inspiration-content--width50">
            <section class="bloc-page">
                <article class="inspiration-content--flex__grid">
                    <h4><?php the_title() ?></h4>
                    <?php the_excerpt() ?>
                    <a href="<?php the_permalink() ?>" class="inspiration-content--style-bottom">Voir nos inspirations</a>
                </article>
            </section>
        </section>
    </section>
<?php endif; ?>