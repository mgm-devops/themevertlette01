<?php

get_header();
get_template_part("template-parts/contact/part", "contact_map");
get_template_part("template-parts/contact/part", "contact_us");
get_template_part("template-parts/contact/part", "contact_form");
get_footer();
