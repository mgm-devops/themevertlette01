<!-- Section  -->
<section class="bloc-page selected-content">
    <section>
        <h3>Notre sélection prestige</h3>
        <span class="selected-content--border__line2"></span>
    </section>
    <section class="products-content">
        <section class="flex-grid">
            <section class="products-content--card">
                <section>
                    <img src="http://vertlette.dev.local/wp-content/uploads/2020/09/dining-room-2157778_1280.jpg" alt="mobilier">
                </section>
                <aside class="products-content--text">
                    <h4>Lorem</h4>
                    <p>100,00$</p>
                </aside>
            </section>

            <section class="products-content--card">
                <section>
                    <img src="http://vertlette.dev.local/wp-content/uploads/2020/09/indoor-4148891_1280.jpg" alt="mobilier">
                </section>
                <aside class="products-content--text">
                    <h4>Lorem</h4>
                    <p>100,00$</p>
                </aside>
            </section>

            <section class="products-content--card">
                <section>
                    <img src="http://vertlette.dev.local/wp-content/uploads/2020/09/room-89020_1280.jpg" alt="mobilier">
                </section>
                <aside class="products-content--text">
                    <h4>Lorem</h4>
                    <p>100,00$</p>
                </aside>
            </section>

            <section class="products-content--card">
                <section>
                    <img src="http://vertlette.dev.local/wp-content/uploads/2020/09/black-and-white-2178783_1280.jpg" alt="mobilier">
                </section>
                <aside class="products-content--text">
                    <h4>Lorem</h4>
                    <p>100,00$</p>
                </aside>
            </section>

        </section>
    </section>
</section>