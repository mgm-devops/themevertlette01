<!-- Section -->
<?php if (have_posts($post = 112)) : ?>
    <section class="bloc-page big-trend-card">
        <section class="big-trend-card--title">
            <h3><?php the_title() ?></h3>
            <p class="big-trend-card--color_text"><span>Source:</span> BrandSource</p>
        </section>
        <section class="flex-grid big-trend">
            <section class="big-trend--width50">
                <?php the_post_thumbnail() ?>
            </section>
            <section class="big-trend--width50">
                <section class="bloc-page">
                    <?php the_content() ?>
                </section>
            </section>
        </section>
    </section>
<?php endif; ?>