<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _kraft
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>

<body>
    <div class="site-container">
        <div class="site-pusher">
            <header id="masthead" class="site-header">
                <section class="background-information-top">
                    <section class="bloc-page flex-grid informationTop-content">
                        <a class="informationTop-content--locate-store" href="http://vertlette.dev.local/trouvez-un-magasin/"><i class="fas fa-map-marker-alt"></i>Trouver un magasin</a>
                        <p class="informationTop-content--text">Livraison gratuite sur toutes les commandes de plus de 2500$!</p>
                        <?php
                        wp_nav_menu(
                            array(
                                'theme_location' => 'header-top',
                                'container'      => 'false',
                                'menu_id'        => 'secondary-menu',
                                'menu_class'     => 'nav'
                            )
                        );
                        ?>
                    </section>
                </section>
                <section class="bloc-page flex-grid sidebar-content">
                    <a href="<?= home_url('/'); ?>" title="<?= __('Homepage', '_themevertlette') ?>">
                        <img class="sidebar-content--logo" src="<?= get_theme_mod('logo') ?>" alt="logo">
                    </a>
                    <section>
                        <section class="flex-grid">
                            <?php
                            wp_nav_menu([
                                'theme_location' => 'header',
                                'menu_id'        => 'primary-menu',
                                'menu_class'     => 'main-navigation'
                            ]);
                            ?>
                            <?php
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'header-top',
                                    'container'      => 'false',
                                    'menu_id'        => 'thirdy-menu',
                                    'menu_class'     => 'nav1'
                                )
                            );
                            ?>
                            <span><i class="fas fa-search"></i></span>

                            <!-- <?php get_search_form() ?> -->
                            <span class="menu_burger">
                                <a href="#" class="header__icon" id="header__icon"></a>
                            </span>
                        </section>
                    </section>
                </section>
            </header><!-- #masthead -->