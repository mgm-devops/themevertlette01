<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _kraft
 */

?>

<footer id="colophon" class="site-footer">
    <section class="background-newsletter">
        <section class="bloc-page flex-grid newsletter-content">
            <aside class="width50 newsletter-content--text">
                <h4>Souscrire à l'infolettre</h4>
                <p>Restez informés de nos derniers arrivages, recevez des conseils tendance et des offres exclusives!
                </p>
            </aside>

            <form id="newsletter" class="width50 mailing" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>"
                method="POST">
                <label class="mailing--input_wrap" for="mailing">
                    <input id=mailing name=mailing type=email placeholder="Saississez votre courriel"
                        class="input-form">
                </label>
                <button class="mailing--style-bottom" name="formNewsletter" type="submit">Inscrivez-vous</button>
            </form>
        </section>
    </section>
    <section class="background-description">
        <section class="bloc-page flex-grid description-content">
            <?php dynamic_sidebar('footer') ?>
            <section class="description-content--social">
                <h4 class="description__title">Suivez-nous</h4>
                <span class="flex-grid">
                    <a href=""><i class="fab fa-facebook-f"></i></a>
                    <a href=""><i class="fas fa-rss"></i></a>
                    <a href=""><i class="fab fa-twitter"></i></a>
                    <a href=""><i class="fab fa-pinterest-p"></i></a>
                    <a href=""><i class="fab fa-instagram"></i></a>
                </span>
            </section>
        </section>
    </section>
    <section class="background-copyright">
        <section class="bloc-page flex-grid copyright-content">
            <aside class="flex-grid copyright-content--text">
                <p>Copyright @ 2020 Luxembert .Tous droits réservés</p>
                <span class="border-line"></span>
                <p><a href="http://vertlette.dev.local/politique-de-confidentialite/">Politique de confidentialité</a>
                </p>
                <p><a href="http://vertlette.dev.local/terme-et-condition/">Terme et condition</a></p>
                <span class="copyright-content--payment-card">
                    <a href=""><i class="fab fa-cc-visa"></i></a>
                    <a href=""><i class="fab fa-cc-amex"></i></a>
                    <a href=""><i class="fab fa-cc-mastercard"></i></a>
                </span>
            </aside>
        </section>
    </section>
</footer><!-- #colophon -->
<div class="site-cache" id="site-cache"></div>
</div> <!-- site-container -->
</div> <!-- site-pusher -->
<?php wp_footer(); ?>

</body>

</html>