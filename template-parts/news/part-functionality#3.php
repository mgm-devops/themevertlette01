<!-- section -->
<?php if (have_posts($post = 116)) : ?>
    <section class="bloc-page big-trend-card">
        <section class="big-trend-card--title">
            <h3><?php the_title() ?></h3>
        </section>
        <section class="flex-grid big-trend">
            <section class="big-trend--width50">
                <section class="big-trend--bloc-page">
                    <?php the_content() ?>
                </section>
            </section>
            <section class="big-trend--width50">
                <?php the_post_thumbnail() ?>
            </section>
        </section>
    </section>
<?php endif; ?>