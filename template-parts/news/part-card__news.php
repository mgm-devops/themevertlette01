<!-- Section  -->
<section class="bloc-page news">
    <article class="flex-grid">
        <section class="news--card">
            <section>
                <img src="http://vertlette.dev.local/wp-content/uploads/2020/09/living-room-1530301_1280.jpg" alt="mobilier">
            </section>
            <a href="<?php echo get_the_permalink($id = 239) ?>"><?php echo get_the_title($id = 239) ?></a>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
        </section>
        <section class="news--card">
            <section>
                <img src="http://vertlette.dev.local/wp-content/uploads/2020/09/seating-2004366_1280-1.jpg" alt="mobilier">
            </section>
            <a href="<?php echo get_the_permalink($id = 240) ?>"><?php echo get_the_title($id = 240) ?></a>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
        </section>
        <section class="news--card">
            <section>
                <img src="http://vertlette.dev.local/wp-content/uploads/2020/09/furniture-3368823_1280.jpg" alt="mobilier">
            </section>
            <a href="<?php echo get_the_permalink($id = 241) ?>"><?php echo get_the_title($id = 241) ?></a>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
        </section>
    </article>
</section>