<?php

namespace App;

/**
 * Enqueue scripts and styles.
 */
class Enqueues
{
    public function __construct()
    {
        add_action('wp_enqueue_scripts', [$this, 'themevertlette_scripts']);
        add_action('wp_enqueue_scripts', [$this, 'woocommerce_style']);
    }

    public function themevertlette_scripts()
    {
        wp_enqueue_style('_themevertlette-style', get_stylesheet_uri(), array(), _S_VERSION);
        wp_enqueue_style('font-awesome', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css');
        wp_style_add_data('_themevertlette-style', 'rtl', 'replace');

        wp_enqueue_script('_themevertlette-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true);

        wp_deregister_script('jquery');
        wp_register_script('jquery', 'http://code.jquery.com/jquery-3.5.1.js');
        wp_enqueue_script('jquery');

        if (is_singular() && comments_open() && get_option('thread_comments')) {
            wp_enqueue_script('comment-reply');
        }
    }

    public function woocommerce_style()
    {
        wp_register_style('mytheme-woocommerce', get_template_directory_uri() . '/woocommerce.css');

        if (class_exists('woocommerce')) {
            wp_enqueue_style('mytheme-woocommerce');
        }
    }
}
new Enqueues();
