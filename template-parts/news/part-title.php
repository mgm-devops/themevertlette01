<!-- Section  -->
<main id="primary" class="site-main">
    <section class="bloc-page title">
        <section>
            <h2 class="newspaper-h2"><?php single_post_title() ?></h2>
        </section>
        <section class="title--auto__center">
            <p class="title--quote">Les nouveautés et tendance pour 2020</p>
            <span class="title--border__line"></span>
        </section>
    </section>