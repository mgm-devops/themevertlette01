<!-- </section> -->
<?php if (have_posts($post = 77)) : ?>
    <section class="flex-grid prestige-content">
        <section class="prestige-content--width50">
            <section class="bloc-page">
                <article class="prestige-content--flex__grid">
                    <h4><?php the_title() ?></h4>
                    <?php the_excerpt() ?>
                    <a href="<?php the_permalink() ?>" class="prestige-content--style-bottom">Lire la suite</a>
                </article>
            </section>
        </section>
        <section class="prestige-content--width50">
            <?php the_post_thumbnail() ?>
        </section>
    </section>
<?php endif; ?>