<section class="bloc-page flex-grid">
    <section class="contact-page">
        <br>
        <br>
        <em>
            <h2>Contactez-nous</h2>
            <p>
                175 Chemin des pionniers Ouest, <br>
                Cap-st-Ignace, Québec, Canada, G0R 1H0
                <p>
                    Téléphone: <strong>000-000-0000</strong> <br>
                    Fax: <strong>000-000-0000</strong> <br>
                    Courriel: <a href="#">info@luxembert.com</a> <br>
                    <br>
                    Sans frais: <strong>1-000-000-0000</strong> <br>
                </p>

                <h2>Heure d'ouverture</h2>
                <p>
                    Lundi au Vendredi: 10:00-17:30PM <br>
                    Samedi: 10:00-17:00PM <br>
                    Dimanche: Fermé <br>
                </p>
        </em>
    </section>