<!-- Section  -->
<section class="bloc-page title">
    <section>
        <h1 class="title--h1"><span class="title--color__gold">Luxe</span>mbert</h1>
    </section>
    <section class="title--auto__center">
        <p class="title--quote">Une nouvelle ère pour le mobilier de prestige</p>
        <span class="title--border__line"></span>
        <p class="title--text">Bienvenue chez Luxembert, pionnier de renom au sein l’industrie de l’ameublement
            haut de gamme et chef de file en matière de service de qualité, tant exceptionnel
            qu’unique.</p>
    </section>
</section>